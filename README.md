# README #

Short course on algorithm.

### What are your plans? ###

* Modification of the best algorithm CryptoNote and fixes of its shortcomings, including the addition of new technologies.

### Where will the algorithm be applied? ###

* Soon, the usual currency will go out of fashion, as well as for a number of multiple reasons (for example, centralization). The currency of the future should not be managed in any way by anyone, it must manage all its users (the principle of democracy or socialism). Crypto currency is the future in the world of economy because all transactions are unrealistically fast, and also safe.

### Why exactly should your algorithm prevail in the market of crypto currency? ###

* The Relativity algorithm is:
- The fastest
- Safe
- Anonymous
- Independent of the states
- Reliable
- Decentralized
- Easy to mine
- With the least commission (even across continents with high speed)
- With a limited number of coins (there will be no inflation)

### Who is on your team? ###

* It sounds funny, but I'm alone.